from cookiecontrol.models import CookieControlConfig
from django.contrib import admin

class CookieControlConfigAdmin(admin.ModelAdmin):

    fieldsets = (
        ('Disclaimers', {
            'fields': ('intro_text', 'full_text')
        }),
        ('Configuration', {
            'fields': ('consent_model', ('position', 'shape', 'theme'), ('start_open', 'auto_hide'), 'subdomains', 'countries')
        }),
        ('Google Analytics', {
            'fields': ('ga_enabled', 'ga_id')
        }),
        ('Piwik', {
            'fields': ('piwik_enabled', 'piwik_base_url', 'piwik_site_id')
        }),
        ('AddThis', {
            'fields': ('addthis_allow_cookies',)
        })
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(CookieControlConfig, CookieControlConfigAdmin)